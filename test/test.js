const chai = require("chai");
const chaiHTTP = require("chai-http");
const app = require("../app");

chai.use(chaiHTTP);
chai.should();

describe("TODOS", () => {
  describe("CRUD", () => {
    it("should get all todos", done => {
      chai
        .request(app)
        .get("/todos")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    const todo = {
      _id: 3,
      name: "New todo",
      completed: false
    };

    it("should add todo", done => {
      chai
        .request(app)
        .post("/todos/")
        .send(todo)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });

    it("should get a single todo", done => {
      const id = 3;
      chai
        .request(app)
        .get(`/todos/${id}`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it("should delete a single todo", done => {
      const id = 3;
      chai
        .request(app)
        .delete(`/todos/${id}`)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });

    it("should update todo", done => {
      const updatedTodo = {
        id: 2,
        title: "Second todo",
        description: "Updated description"
      };
      chai
        .request(app)
        .put(`/todos/2`)
        .send(updatedTodo)
        .end((err, result) => {
          result.should.have.status(201);
          done();
        });
    });
  });
});
