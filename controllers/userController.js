const db = require("../models");
const bcrypt = require("bcryptjs");
const keys = require("../config/keys");
const jwt = require("jsonwebtoken");
const validateSignup = require("../validation/validateSignup");
const validateLogin = require("../validation/validateLogin");

exports.signup = (req, res) => {
  const { errors, isValid } = validateSignup(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  db.User.findOne({ where: { email: req.body.email } }).then(user => {
    if (user) {
      return res.status(400).json({ email: "Email already exists!" });
    } else {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) throw err;
          db.User.create({
            email: req.body.email,
            password: hash
          })
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
    }
  });
};

exports.login = (req, res) => {
  const { errors, isValid } = validateLogin(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;
  db.User.findOne({ where: { email: email } }).then(user => {
    if (!user) {
      return res.status(404).json({ email: "Email not found!" });
    }

    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        const payload = {
          id: user.id,
          email: user.email
        };
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res.status(400).json({ password: "Password incorrect" });
      }
    });
  });
};

exports.getAllUsers = (req, res) => {
  db.User.findAll().then(result => res.status(200).json(result));
};
