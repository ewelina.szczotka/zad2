const db = require("../models");

exports.getAllTodos = (req, res) => {
  db.Todo.findAll().then(result => res.status(200).json(result));
};

exports.getTodo = (req, res) => {
  db.Todo.findByPk(req.params._id)
    .then(result => res.status(200).json(result))
    .catch(err => res.status(404).json(err));
};

exports.createTodo = (req, res) => {
  db.Todo.create({
    _id: req.body._id,
    name: req.body.name,
    completed: req.body.completed
  }).then(result => res.status(201).json(result));
};

exports.updateTodo = (req, res) => {
  db.Todo.update(
    {
      _id: req.body._id,
      name: req.body.name,
      completed: req.body.completed
    },
    {
      where: {
        _id: req.params.id
      }
    }
  ).then(result => res.status(201).json(result));
};

exports.deleteTodo = (req, res) => {
  db.Todo.destroy({
    where: {
      _id: req.params.id
    }
  }).then(result => res.status(201).json(result));
};
