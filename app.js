const express = require("express");
const morgan = require("morgan");
const app = express();
const port = process.env.PORT || 4000;
const userRoutes = require("./routes/users");
const todoRoutes = require("./routes/todos");

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan("dev"));
app.use("/", userRoutes);
app.use("/", todoRoutes);

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Method", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

const models = require("./models");

models.sequelize
  .sync()
  .then(function() {
    console.log("Database is fine");
  })
  .catch(function(err) {
    console.log(err, "Something went wrong");
  });

app.listen(port, () => {
  console.log("Server running on port " + port);
});

module.exports = app;
